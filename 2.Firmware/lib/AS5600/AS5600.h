#ifndef __AS5600__
#define __AS5600__

#include <Arduino.h>
#include <Wire.h>
#include "Config.h"

#define AS5600_IIC_ADDRESS 0x36
#define AS5600_RAW_HIGH 0x0C
#define AS5600_RAW_LOW 0x0D

#define FOC_CH1_FREQ 100000
#define FOC_CH2_FREQ 100000

void AS5600Init(void);
bool checkEncoderCH1(void);
bool checkEncoderCH2(void);
unsigned int getRowFromAS5600_CH1(void);
unsigned int getRowFromAS5600_CH2(void);

#endif
//iic 检测代码
// #include <Wire.h>
 
 
// void setup()
// {
//   Wire.begin(FOC_CH2_SDA_PIN,FOC_CH2_SCL_PIN);
 
//   Serial.begin(115200);
//   while (!Serial);             // Leonardo: wait for serial monitor
//   Serial.println("\nI2C Scanner");
// }
 
 
// void loop()
// {
//   byte error, address;
//   int nDevices;
 
//   Serial.println("Scanning...");
 
//   nDevices = 0;
//   for(address = 1; address < 127; address++ ) 
//   {
//     // The i2c_scanner uses the return value of
//     // the Write.endTransmisstion to see if
//     // a device did acknowledge to the address.
//     Wire.beginTransmission(address);
//     error = Wire.endTransmission();
 
//     if (error == 0)
//     {
//       Serial.print("I2C device found at address 0x");
//       if (address<16) 
//         Serial.print("0");
//       Serial.print(address,HEX);
//       Serial.println("  !");
 
//       nDevices++;
//     }
//     else if (error==4) 
//     {
//       Serial.print("Unknown error at address 0x");
//       if (address<16) 
//         Serial.print("0");
//       Serial.println(address,HEX);
//     }    
//   }
//   if (nDevices == 0)
//     Serial.println("No I2C devices found\n");
//   else
//     Serial.println("done\n");
 
//   delay(1000);           // wait 5 seconds for next scan
// }
